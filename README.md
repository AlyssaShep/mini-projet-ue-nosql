# mini-projet-ue-NoSQL

## Bibliothèques externes
Le projet utilise deux bibliothèques
* rdf4j2 3.7.3 pour lire les données rdf et requêtes sparql
* jena 4 3.9.0 qui sera utilisée pour comparaison dans la phase d’analyse des perfor-
mances

## Dates de rendu

Le projet est divisé en 3 rendus : Dates de rendu
1. dictionnaire et index : rendu du code (pas de rapport) 16 Novembre
2. évaluation des requêtes en étoile : code + rapport 3 pages 2 Décembre
3. analyse des performances : code + rapport 5 pages 17 Décembre


## Évaluation
Les points suivants seront sujet à évaluation.
* Travail réalisé (code fonctionnel, implémentation des fonctionnalités)
* Qualité du code produit
* Clarté et concision du rapport

## Lancement
Rajouter l'execution au script avec la commande :
```bash
chmod +x script_jar.sh
```

puis lancer le script
```bash
./script_jar.sh
```

Maintenant vous pouvez lancer le projet avec les lignes suivantes :

```bash
java -jar rdfengine
-queries "/chemin/vers/dossier/requetes"
-data "/chemin/vers/fichier/donnees"
-output "/chemin/vers/dossier/sortie"
-Jena : active la vérification de la correction et complétude du système
en utilisant Jena comme un oracle
-warm "X" : utilise un échantillon des requêtes en entrée (prises
au hasard) correspondant au pourcentage "X" pour chauffer le système
-shuffle : considère une permutation aléatoire des requêtes en entrée
```
