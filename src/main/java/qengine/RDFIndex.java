package qengine;

import java.util.ArrayList;

public class RDFIndex {
    public ArrayList<Index> rdfIndexSPO = new ArrayList<>();
    public ArrayList<Index> rdfIndexSOP = new ArrayList<>();
    public ArrayList<Index> rdfIndexPSO = new ArrayList<>();
    public ArrayList<Index> rdfIndexOPS = new ArrayList<>();
    public ArrayList<Index> rdfIndexPOS = new ArrayList<>();
    public ArrayList<Index> rdfIndexOSP = new ArrayList<>();

    public long tempsCreationIndex = 0;

    public void setIndex(int subject, int predicate, int object){
        long startTime = System.currentTimeMillis();

        rdfIndexSPO.add(new Index(subject, predicate, object));
        rdfIndexSOP.add(new Index(subject, object, predicate));
        rdfIndexPSO.add(new Index(predicate, subject, object));
        rdfIndexOPS.add(new Index(object, predicate, subject));
        rdfIndexPOS.add(new Index(predicate, object, subject));
        rdfIndexOSP.add(new Index(object, subject, predicate));

        tempsCreationIndex += System.currentTimeMillis() - startTime;
    }

    public void toStringPrint(){
        System.out.println("Index SPO : ");
        for (Index ind : rdfIndexSPO){
            String indexes = "subject : " + String.valueOf(ind.getElement1()) + "\t predicate : " + String.valueOf(ind.getElement2()) + "\t object : " + String.valueOf(ind.getElement3()) + "\n";

            System.out.println(indexes);
        }

        System.out.println("Index SOP : ");
        for (Index ind : rdfIndexSOP){
            String indexes = "subject : " + String.valueOf(ind.getElement1()) + "\t object : " + String.valueOf(ind.getElement3()) + "\t predicate : " + String.valueOf(ind.getElement2()) + "\n";

            System.out.println(indexes);
        }

        System.out.println("Index PSO : ");
        for (Index ind : rdfIndexPSO){
            String indexes = "predicate : " + String.valueOf(ind.getElement2()) + "\t subject : " + String.valueOf(ind.getElement1()) + "\t object : " + String.valueOf(ind.getElement3()) + "\n";

            System.out.println(indexes);
        }

        System.out.println("Index OPS : ");
        for (Index ind : rdfIndexOPS){
            String indexes = "object : " + String.valueOf(ind.getElement3()) + "\t predicate : " + String.valueOf(ind.getElement2()) + "\t subject : " + String.valueOf(ind.getElement1()) + "\n";

            System.out.println(indexes);
        }

        System.out.println("Index POS : ");
        for (Index ind : rdfIndexPOS){
            String indexes = "predicate : " + String.valueOf(ind.getElement2()) + "\t object : " + String.valueOf(ind.getElement3()) + "\t subject : " + String.valueOf(ind.getElement1()) + "\n";

            System.out.println(indexes);
        }

        System.out.println("Index OSP : ");
        for (Index ind : rdfIndexOSP){
            String indexes = "object : " + String.valueOf(ind.getElement3()) + "\t subject : " + String.valueOf(ind.getElement1()) + "\t predicate : " + String.valueOf(ind.getElement2()) + "\n";

            System.out.println(indexes);
        }
    }

}
