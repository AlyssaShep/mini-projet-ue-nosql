package qengine;

import org.eclipse.rdf4j.model.Statement;
import org.eclipse.rdf4j.rio.helpers.AbstractRDFHandler;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Le RDFHandler intervient lors du parsing de données et permet d'appliquer un traitement pour chaque élément lu par le parseur.
 * 
 * <p>
 * Ce qui servira surtout dans le programme est la méthode {@link #handleStatement(Statement)} qui va permettre de traiter chaque triple lu.
 * </p>
 * <p>
 * À adapter/réécrire selon vos traitements.
 * </p>
 */
public final class MainRDFHandler extends AbstractRDFHandler {


	//	dictionnaire pour les fichiers data
//	utilisation de LinkedHashMap pour conserver l ordre d insertion
	static Map<Integer, String> dictionary = new LinkedHashMap<>();
	static Map<String, Integer> revDictionary = new LinkedHashMap<>();

	static RDFIndex rdfIndex = new RDFIndex();

	static int id = 0;

	static long tempsCreationDico = 0;

	public long sizeDic(){
		return dictionary.size();
	}

	public long getTempsCreationDico(){
		return tempsCreationDico;
	}

	public long getTempsCreationnIndex(){
		return rdfIndex.tempsCreationIndex;
	}

	public RDFIndex getIndex(){
		return rdfIndex;
	}

	public Map<String, Integer> getRevDictionary(){
		return revDictionary;
	}

	public int getKey(String value){
		if (revDictionary.get(value) == null){
			return -1;
		}
		return revDictionary.get(value);
	}

	public Integer getKeyDictionary(String value) {
		for (Map.Entry<Integer, String> entry : dictionary.entrySet()) {
//			System.out.println("Dictionnaire  valeur : " + entry.getValue() + " valeur : " + value);
			if (value.equals(entry.getValue())) {
//				System.out.println("Dictionnaire  valeur : " + entry.getValue() + " avec clef : " + entry.getKey() + " valeur : " + value);
				return entry.getKey();
			}
		}
		return -1; // Retourne null si la valeur n'est pas trouvée
	}

	public String getValue(Integer key){
		return dictionary.get(key);
	}

	public void toStringPrint(){
		System.out.println("\nDictionnaire : ");
		for (Map.Entry<Integer, String> entry : dictionary.entrySet()) {
			String dictionaryString = "\tclef : " + entry.getKey() + "\tvaleur : " + entry.getValue() + "\n";
			System.out.println(dictionaryString);
		}

		System.out.println("\nIndex : ");
		rdfIndex.toStringPrint();

	}

	@Override
	public void handleStatement(Statement st) {

		long startTime = System.currentTimeMillis();

		// ajout au dictionnaire
		// on fait bien attention que les entrees n existent pas deja
		boolean containsSubject = revDictionary.containsKey(String.valueOf(st.getSubject()).replace("<", "").replace(">", "").replace("\"", ""));
		boolean containsPredicate = revDictionary.containsKey(String.valueOf(st.getPredicate()).replace("<", "").replace(">", "").replace("\"", ""));
		boolean containsObject = revDictionary.containsKey(String.valueOf(st.getObject()).replace("<", "").replace(">", "").replace("\"", ""));

		int temp1 = 0, temp2 = 0, temp3 = 0;

		if (!containsSubject){
			String tempSt = String.valueOf(st.getSubject()).replace("<", "").replace(">", "").replace("\"", "");
			dictionary.put(id,  tempSt);
			revDictionary.put(tempSt, id);
			temp1 = id;
			id++;
		}

		if (!containsPredicate){
			String tempSt = String.valueOf(st.getPredicate()).replace("<", "").replace(">", "").replace("\"", "");
			dictionary.put(id, tempSt );
			revDictionary.put(tempSt, id);
			temp2 = id;
			id++;
		}

		if (!containsObject){
			String tempSt = String.valueOf(st.getObject()).replace("<", "").replace(">", "").replace("\"", "");
			dictionary.put(id, tempSt );
			revDictionary.put(tempSt, id);
			temp3 = id;
			id++;
		}

		// on recupere les cles du dictionnaire si les valeurs existent deja
		if (temp1 == 0) {
			temp1 = revDictionary.get(String.valueOf(st.getSubject()).replace("<", "").replace(">", "").replace("\"", ""));
		}

		if (temp2 == 0) {
			temp2 = revDictionary.get(String.valueOf(st.getPredicate()).replace("<", "").replace(">", "").replace("\"", ""));
		}

		if (temp3 == 0) {
			temp3 = revDictionary.get(String.valueOf(st.getObject()).replace("<", "").replace(">", "").replace("\"", ""));
		}

		// ajout de la relation dans l index spo ops pos (6 permutations)
		rdfIndex.setIndex(temp1, temp2, temp3);

		// affichage par etape ( a décommenter si besoin pour voir les etapes)

//		System.out.println("\n" + st.getSubject() + "\t " + st.getPredicate() + "\t " + st.getObject());
		// on verifie que les données soient bien la en les affichant
//		System.out.println("Valeurs du dictionnaire : " + dictionary);

		// on peut afficher les dernieres valeurs ajoutees
//		System.out.println("\n Dernieres valeurs ajoutees : " + dictionary.get(temp1) + "\t " + dictionary.get(temp2) + "\t " + dictionary.get(temp3));

		// affichage de l index sur le dernier ajout
//		System.out.println(rdfIndex.toStringLast());

		tempsCreationDico += System.currentTimeMillis() - startTime;

	}


}