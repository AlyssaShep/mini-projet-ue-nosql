package qengine;

import java.io.*;
import java.lang.reflect.Array;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Stream;

import org.eclipse.rdf4j.query.algebra.In;
import org.eclipse.rdf4j.query.algebra.Projection;
import org.eclipse.rdf4j.query.algebra.ProjectionElem;
import org.eclipse.rdf4j.query.algebra.StatementPattern;
import org.eclipse.rdf4j.query.algebra.helpers.AbstractQueryModelVisitor;
import org.eclipse.rdf4j.query.algebra.helpers.StatementPatternCollector;
import org.eclipse.rdf4j.query.parser.ParsedQuery;
import org.eclipse.rdf4j.query.parser.sparql.SPARQLParser;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.RDFParser;
import org.eclipse.rdf4j.rio.Rio;

/**
 * Programme simple lisant un fichier de requête et un fichier de données.
 * 
 * <p>
 * Les entrées sont données ici de manière statique,
 * à vous de programmer les entrées par passage d'arguments en ligne de commande comme demandé dans l'énoncé.
 * </p>
 * 
 * <p>
 * Le présent programme se contente de vous montrer la voie pour lire les triples et requêtes
 * depuis les fichiers ; ce sera à vous d'adapter/réécrire le code pour finalement utiliser les requêtes et interroger les données.
 * On ne s'attend pas forcémment à ce que vous gardiez la même structure de code, vous pouvez tout réécrire.
 * </p>
 * 
 * @author Olivier Rodriguez <olivier.rodriguez1@umontpellier.fr>
 */
final class Main {
	static final String baseURI = null;

	/**
	 * Votre répertoire de travail où vont se trouver les fichiers à lire
	 */
	static final String workingDir = "data/";

	/**
	 * Fichier contenant les requêtes sparql
	 */
	static String queryFile = workingDir + "sample_query.queryset";

	static String queryDir = "data/";

	static boolean dirGivenForQueries = false;

	/**
	 * Fichier contenant des données rdf
	 */
	static String dataFile = workingDir + "sample_data.nt";

	static String outputFile = "output.txt";
	static FileWriter csvWriter;

	static String evalFile = "evalFile.csv";
	
	static String csvFilePath = "queries_res.csv";

//	static PrintWriter writer;

	static float warm = -1;

	static int numberQueries = -1;

	static boolean shuffle = false;

	static MainRDFHandler dictionaryIndex = new MainRDFHandler();

	static String requeteEnCours = "";

	static long NombreTotalRequetes = 0;

	static long nb_RDF_triples = 0;

	static long requeteAvecReponse = 0;

	// temps
	static long tempsTotalEvaluation = 0;
	static long tempsLectureDonnees = 0;
	static long tempsLectureRequetes = 0;
	static long tempsTotal = 0;

	static long startTimeQueries = 0;

	// ========================================================================

	/**
	 * Méthode utilisée ici lors du parsing de requête sparql pour agir sur l'objet obtenu.
	 */
	public static void processAQuery(ParsedQuery query, boolean warmup) {
		List<StatementPattern> patterns = StatementPatternCollector.process(query.getTupleExpr());

		// execution de la requete
		// on regarde qui manque dans la requete
		// on recupere les valeurs de clef dans le dictionnaire
		// on fait la recherche

		// on va creer une nombre n de sous-index par sous-requete
		// car pour chaque sous-requete on recherchera sur 2 index a chaque fois
		// par exemple s il manque le subject on fera la recherche dans pos et ops
		int size = patterns.size();
		ArrayList<ArrayList<Index>> answers = new ArrayList<>(size);
		for (int i = 0; i < size; i++){
			answers.add(new ArrayList<>());
		}

		// pour chaque requete on va conserver le resultat de la requete dans answer a son numero de sous-requete
		// une fois qu on aura ses donnees on pourra les croiser et recuperer la valeur que l on recherche
		int requete_traitee = 0;

		RDFIndex tempIndex = dictionaryIndex.getIndex();
		for (StatementPattern pattern : patterns) {

			// d abord on recherche dans le dictionnaire la valeur attribuee a cette element
			// si l element n appartient pas on peut directement passer a la sous-requete suivante
			int subject = pattern.getSubjectVar().getValue() == null ? -1 : dictionaryIndex.getKey(pattern.getSubjectVar().getValue().stringValue());
			int predicate = pattern.getPredicateVar().getValue() == null ? -1 : dictionaryIndex.getKey(pattern.getPredicateVar().getValue().stringValue());
			int object = pattern.getObjectVar().getValue() == null ? -1 : dictionaryIndex.getKey(pattern.getObjectVar().getValue().stringValue());

			if ((subject == -1 && predicate == -1) || (subject == -1 && object == -1) || (predicate == -1 && object == -1)){
				requete_traitee++;
				continue;
			}

			if(pattern.getSubjectVar().getValue() == null){
				// si c est la premiere sous-requete, on fait la recherche sans croiser avec les resultats precedents
				// on fait la recherche dans les index pos et ops

				for(Index pos : tempIndex.rdfIndexPOS){
					if((pos.getElement1() == predicate) && (pos.getElement2() == object)){
						// on ajoute aux elements de reponses
						answers.get(requete_traitee).add(pos);
					}
				}

//				for(Index ops : tempIndex.rdfIndexOPS){
//					if((ops.getElement1() == object) && (ops.getElement2() == predicate)){
//						// on ajoute aux elements de reponses
//						answers.get(requete_traitee).add(ops);
//					}
//				}


			} else if (pattern.getPredicateVar().getValue() == null){

				// on fait la recherche dans les index osp et sop
				for(Index osp : tempIndex.rdfIndexOSP){
					if((osp.getElement1() == object) && (osp.getElement2() == subject)){
						// on ajoute aux elements de reponses
						answers.get(requete_traitee).add(osp);
					}
				}

//				for(Index sop : tempIndex.rdfIndexSOP){
//					if((sop.getElement1() == subject) && (sop.getElement2() == object)){
//						// on ajoute aux elements de reponses
//						answers.get(requete_traitee).add(sop);
//					}
//				}

			} else {

				// on fait la recherche dans les index pso et pso
				for(Index pso : tempIndex.rdfIndexPSO){
					if((pso.getElement1() == predicate) && (pso.getElement2() == subject)){
						// on ajoute aux elements de reponses
						answers.get(requete_traitee).add(pso);
					}
				}

//				for(Index pso : tempIndex.rdfIndexPSO){
//					if((pso.getElement1() == predicate) && (pso.getElement2() == subject)){
//						// on ajoute aux elements de reponses
//						answers.get(requete_traitee).add(pso);
//					}
//				}

			}

			requete_traitee++;
		}

		// on croise les donnees pour le resultat final
		ArrayList<Index> result = new ArrayList<>();

		if (size == 1){
			result = answers.get(0);

		} else {
			int temp = 0;
			for(ArrayList<Index> idxa : answers){
				for(Index ida : idxa){

					int tempd = 0;
					for(ArrayList<Index> idx : answers){
						// on evite lui meme
						if (tempd == temp){
							tempd++;
							continue;
						}

						for (Index id : idx){

							// l element qui recherche est le dernier de l index donc on a juste a compare cet element
							if (id.getElement3() == ida.getElement3()){
								result.add(id);
							}

						}
						tempd++;
					}

				}
				temp++;
			}
		}

		// on retire les doublons
		ArrayList<Index> res = new ArrayList<>();
		for (Index index : result){

			boolean same = false;

			for (Index id : res){
				if (id.getElement3() == index.getElement3()){
					same = true;
				}
			}

			if (!same){
				res.add(index);
				same = false;
			}
		}


		// si c est pas de l echauffement, on peut exporter
		if (!warmup){
			exporterDansCSV(requeteEnCours, res);

			if (!result.isEmpty()){
//			System.out.println("Reponse(s) de la requete : ");
				requeteAvecReponse++;
//			for (Index aw : res){
//				System.out.println("\t" + dictionaryIndex.getValue(aw.getElement3()));
//			}
			}
		}

//		System.out.println("--------------------------------------------------------------------------");

	}

	public static void exporterDansCSV(String requete, ArrayList<Index> triplets) {

		try (FileWriter csvWriter = new FileWriter(csvFilePath, true)) {
			StringBuilder ligne = new StringBuilder();

			// ajout du nom du fichier de requetes traitee
			ligne.append(queryFile).append(",");

			// Ajout de la requête traitée à la ligne
			ligne.append(requete).append(",");

			// Ajout des triplets à la ligne (ou "0" si le triplet est vide)
			if (triplets.isEmpty()){
				ligne.append("0").append(",");
			} else {

				for (Index triplet : triplets) {
					ligne.append("<").append(dictionaryIndex.getValue(triplet.getElement1())).append(" ").append(dictionaryIndex.getValue(triplet.getElement2())).append(" ").append(dictionaryIndex.getValue(triplet.getElement3())).append(">").append(",");
				}

			}

			// Supprimer la dernière virgule de la ligne et ajouter un saut de ligne
			ligne.deleteCharAt(ligne.length() - 1).append("\n");

			// Écrire la ligne dans le fichier CSV
			csvWriter.append(ligne.toString());

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Entrée du programme
	 */
	public static void main(String[] args) throws Exception {

		long startTotal = System.currentTimeMillis();

		for (int i = 0; i < args.length; i++) {
			if (args[i].equals("-help")) {
				System.out.println("java -jar rdfengine\n" +
						"-queries \"/chemin/vers/dossier/requetes\"\n" +
						"-data \"/chemin/vers/fichier/donnees\"\n" +
						"-output \"/chemin/vers/dossier/sortie\"\n (chemin du fichier pour les evaluations de performances)" +
						"-extract \"/chemin/vers/dossier/sortie\"\n (chemin du fichier pour les resultats de requetes)" +
						"-Jena : active la vérification de la correction et complétude du système\n" +
						"en utilisant Jena comme un oracle\n" +
						"-warm \"X\" : utilise un échantillon des requêtes en entrée (prises\n" +
						"au hasard) correspondant au pourcentage \"X\" pour chauffer le système\n" +
						"-shuffle : considère une permutation aléatoire des requêtes en entrée");
			}
			if (args[i].equals("-queries") && (i + 1 < args.length)) {
//				queryFile = args[i+1];
				if (!args[i + 1].endsWith("/")){
					System.out.println("-queries doit être un dossier");
					return;
				}
				dirGivenForQueries = true;
				queryDir = args[i + 1];
				continue;
			}
			if (args[i].equals("-data") && (i + 1 < args.length)) {
				dataFile = args[i + 1];
				System.out.println(dataFile);
				continue;
			}
			if (args[i].equals("-extract") && (i + 1 < args.length)) {
				outputFile = args[i + 1];
//				writer = new PrintWriter(outputFile);
				continue;
			}
			if (args[i].equals("-output") && (i + 1 < args.length)) {
				evalFile = args[i + 1];
				csvWriter = new FileWriter(evalFile);
				continue;
			}
			if (args[i].equals("-warm") && (i + 1 < args.length)) {
				warm = Integer.parseInt(args[i + 1]);
				// si warm est different de -1 alors on calculera au moment de la recuperation le nombre de requetes a traiter
				continue;
			}
			if (args[i].equals("-shuffle")) {
				shuffle = true;
			}
		}

//		System.out.println("queries : " + queryFile + " data : " + dataFile + " output : " +  outputFile);

		long startTime = System.currentTimeMillis();

		parseData();
		tempsLectureDonnees = System.currentTimeMillis() - startTime;

		nb_RDF_triples = dictionaryIndex.sizeDic();

		if (dirGivenForQueries){

			File dossier = new File(queryDir);

			if (dossier.isDirectory() && dossier.exists()) {
				File[] fichiers = dossier.listFiles();

				if (fichiers != null) {

					for (File fichier : fichiers) {

						if (fichier.isFile()) {
							requeteAvecReponse = 0;
							NombreTotalRequetes = 0;
							queryFile = queryDir + fichier.getName();

							System.out.println(queryFile);

							startTime = System.currentTimeMillis();

							parseQueries();

							tempsTotalEvaluation = System.currentTimeMillis() - startTime;

//							writer.close();

							tempsTotal = System.currentTimeMillis() - startTotal;

							String evalFileP = evalFile.replace(".queryset", ".csv");

							exporterDansCSV(evalFileP, dataFile, queryFile, nb_RDF_triples, tempsLectureDonnees,
									tempsLectureRequetes, dictionaryIndex.getTempsCreationDico(), 6, dictionaryIndex.getTempsCreationnIndex(), tempsTotalEvaluation, tempsTotal, requeteAvecReponse, NombreTotalRequetes);

						}
					}
				}
			}

		} else {

				startTimeQueries = System.currentTimeMillis();

				parseQueries();

				tempsTotalEvaluation = System.currentTimeMillis() - startTime;

//				writer.close();

				tempsTotal = System.currentTimeMillis() - startTotal;

				exporterDansCSV(evalFile, dataFile, queryFile, nb_RDF_triples, tempsLectureDonnees,
						tempsLectureRequetes, dictionaryIndex.getTempsCreationDico(), 6, dictionaryIndex.getTempsCreationnIndex(), tempsTotalEvaluation, tempsTotal, requeteAvecReponse, NombreTotalRequetes);

		}
	}

	public static void exporterDansCSV(String fichierExport, String nomFichier, String nomDossier, long nbRdfTriples ,long tempsLectureDonnees,
									   long tempsLectureRequetes, long tempsCreationDico, long nbIndex,long tempsCreationIndex,
									   long tempsTotalEvaluation, long tempsTotal, long requeteAvecReponse, long NombreTotalRequetes) {
		try {
			FileWriter csvWriter = new FileWriter(fichierExport, true);
			File fichier = new File(fichierExport);
			StringBuilder sb = new StringBuilder();

			if (fichier.length() == 0){
				sb.append("nomFichier").append(",")
						.append("nomDossier").append(",")
						.append("nbRdfTriples").append(",")
						.append("tempsLectureDonnees").append(",")
						.append("tempsLectureRequetes").append(",")
						.append("tempsCreationDico").append(",")
						.append("nbIndex").append(",")
						.append("tempsCreationIndex").append(",")
						.append("tempsTotalEvaluation").append(",")
						.append("tempsTotal").append(",")
						.append("NombreTotalRequetes").append(",")
						.append("requeteAvecReponse").append(",").append("\n");
			}

			sb.append(nomFichier).append(",")
					.append(nomDossier).append(",")
					.append(nbRdfTriples == 0 ? "NON_DISPONIBLE" : nbRdfTriples).append(",")
					.append(tempsLectureDonnees == 0 ? "NON_DISPONIBLE" : tempsLectureDonnees ).append(",")
					.append(tempsLectureRequetes == 0 ? "NON_DISPONIBLE" : tempsLectureRequetes ).append(",")
					.append(tempsCreationDico == 0 ? "NON_DISPONIBLE" : tempsCreationDico).append(",")
					.append(nbIndex == 0 ? "NON_DISPONIBLE" : nbIndex).append(",")
					.append(tempsCreationIndex == 0 ? "NON_DISPONIBLE" : tempsCreationIndex).append(",")
					.append(tempsTotalEvaluation == 0 ? "NON_DISPONIBLE" : tempsTotalEvaluation ).append(",")
					.append(tempsTotal == 0 ? "NON_DISPONIBLE" : tempsTotal ).append(",")
					.append(NombreTotalRequetes).append(",")
					.append(requeteAvecReponse).append(",").append("\n");

			System.out.println(requeteAvecReponse);

			csvWriter.append(sb.toString());

			csvWriter.flush();
			csvWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	// ========================================================================

	/**
	 * Traite chaque requête lue dans {@link #queryFile} avec {@link #processAQuery(ParsedQuery)}.
	 */
	private static void parseQueries() throws FileNotFoundException, IOException {
		/**
		 * Try-with-resources
		 * 
		 * @see <a href="https://docs.oracle.com/javase/tutorial/essential/exceptions/tryResourceClose.html">Try-with-resources</a>
		 */
		/*
		 * On utilise un stream pour lire les lignes une par une, sans avoir à toutes les stocker
		 * entièrement dans une collection.
		 */
		long startTime = System.currentTimeMillis();
		try (Stream<String> lineStream = Files.lines(Paths.get(queryFile))) {
			ArrayList<String> queriesToRun = new ArrayList<>();
//			SPARQLParser sparqlParser = new SPARQLParser();
			Iterator<String> lineIterator = lineStream.iterator();
			StringBuilder queryString = new StringBuilder();

			while (lineIterator.hasNext())
			/*
			 * On stocke plusieurs lignes jusqu'à ce que l'une d'entre elles se termine par un '}'
			 * On considère alors que c'est la fin d'une requête
			 */
			{
				String line = lineIterator.next();
				queryString.append(line);

				if (line.trim().endsWith("}")) {
//					ParsedQuery query = sparqlParser.parseQuery(queryString.toString(), baseURI);

					queriesToRun.add(queryString.toString());
//					processAQuery(query); // Traitement de la requête, à adapter/réécrire pour votre programme

					queryString.setLength(0); // Reset le buffer de la requête en chaine vide
				}
			}

			tempsLectureRequetes = System.currentTimeMillis() - startTime;

			NombreTotalRequetes = queriesToRun.size();

			runQueries(queriesToRun);

		}
	}

	private static void runQueries(ArrayList<String> queries){
		SPARQLParser sparqlParser = new SPARQLParser();
		if (shuffle){
			Collections.shuffle(queries);
		}

		if (warm < 0) {
			for (String q : queries){
				ParsedQuery query = sparqlParser.parseQuery(q, baseURI);

//				System.out.println("");
//				System.out.println("Requete traitee : ");
//				System.out.println(q);
//				requeteEnCours = q.toString();

				processAQuery(query, false);
			}
		} else {

			long stopForWarmUp = startTimeQueries;

			int size = queries.size();
			// on fait le pourcentage de requetes que l on doit traiter
			numberQueries = Math.round(size * (warm/100));
			Random randomNumbers = new Random();
//			System.out.println("for warmup - taille : " + size +" nombre de queries : " + numberQueries);

			// warm up
			for(int i = 0; i < numberQueries; i++){
				int rd = randomNumbers.nextInt(size);

//				System.out.println("");
//				System.out.println("Requete traitee : ");
//				System.out.println(queries.get(rd));

				ParsedQuery query = sparqlParser.parseQuery(queries.get(rd), baseURI);
				processAQuery(query, true);
			}

			// le warmup ne compte pas dans les evaluations donc on remet a la valeur avant le warmup
			startTimeQueries = stopForWarmUp;

			for (String q : queries){
				ParsedQuery query = sparqlParser.parseQuery(q, baseURI);

//				System.out.println("");
//				System.out.println("Requete traitee : ");
//				System.out.println(q);
//				requeteEnCours = q.toString();

				processAQuery(query, false);
			}
		}

	}

	/**
	 * Traite chaque triple lu dans {@link #dataFile} avec {@link MainRDFHandler}.
	 */
	private static void parseData() throws FileNotFoundException, IOException {

		try (Reader dataReader = new FileReader(dataFile)) {
			// On va parser des données au format ntriples
			RDFParser rdfParser = Rio.createParser(RDFFormat.NTRIPLES);

			// On utilise notre implémentation de handler
			rdfParser.setRDFHandler(dictionaryIndex);
//			rdfParser.setRDFHandler(new MainRDFHandler());

			// Parsing et traitement de chaque triple par le handler
			rdfParser.parse(dataReader, baseURI);
		}
	}
}
