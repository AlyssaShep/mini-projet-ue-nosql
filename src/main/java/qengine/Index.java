package qengine;

public class Index {
    private int element1;

    private int element2;

    private int element3;

    public Index(int s, int p, int o){
        this.element1 = s;
        this.element2 = p;
        this.element3 = o;
    }

    public int getElement1() {
        return element1;
    }

    public void setElement1(int element1) {
        this.element1 = element1;
    }

    public int getElement2() {
        return element2;
    }

    public void setElement2(int element2) {
        this.element2 = element2;
    }

    public int getElement3() {
        return element3;
    }

    public void setElement3(int element3) {
        this.element3 = element3;
    }

    public String toString() {
        return "element 1 : " + element1 + " element 2 : " + element2 + "  element 3 : " + element3;
    }

    public boolean sameIndex(Index index){
        if ( (element1 == index.element1) || (element1 == index.element2) || (element1 == index.element3) ){
            if ( (element2 == index.element1) || (element2 == index.element2) || (element2 == index.element3) ){
                if ( (element3 == index.element1) || (element3 == index.element2) || (element3 == index.element3) ){
                    return true;
                }
            }
        }
        return false;
    }

}
